import React, { useContext, useEffect, useState } from 'react'
import { Layout, Menu, Button, message, Avatar, Dropdown, Space } from 'antd'
import {
  DashboardOutlined
} from '@ant-design/icons'
import Image from 'next/image'
import styles from '../../styles/Dashboard.module.css'
import axios from 'axios'
import { FBAPIContext } from '../_app'
import { useRouter } from 'next/router'
import withPrivateRoute from '../../components/withPrivateRoute'

const { Header, Content, Sider } = Layout

const Dashboard = ({ loading }) => {
  const router = useRouter()
  const [isUpdating, setIsUpdating] = useState(false)
  const { api } = useContext(FBAPIContext)
  const [profile, setProfile] = useState()

  useEffect(async () => {
    if (!loading) {
      const response = await api.api("me?fields=id,name,picture")
      setProfile(response)
    }
  }, [loading, api])

  const updateFBPhotos = async () => {
    setIsUpdating(true)

    try {
      const { authResponse: { accessToken } } = await api.getLoginStatus()
      const response = await axios.get("/api/update", {
        params: {
          accessToken
        }
      })
      message.success(`Updated ${response.data} images`)
    } catch (error) {
      console.log(error)
    } finally {
      setIsUpdating(false)
    }
  }

  const onProfileMenuClick = async ({ key }) => {
    if (key === 'logout') {
      await api.logout()
      router.push('/login')
    }
  }

  const ProfileMenu = (
    <Menu onClick={onProfileMenuClick}>
      <Menu.Item key='logout'>Log out</Menu.Item>
    </Menu>
  )

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider theme="light" className={styles.sider}>
        <div className={styles.logo}>
          <a href="/" rel="noopener">
            <Image src="/images/logo.png" width={142} height={64} />
          </a>
        </div>
        <Menu theme="light" defaultSelectedKeys={['1']} mode="inline">
          <Menu.Item key="1" icon={<DashboardOutlined />}>
            Dashboard
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header className={styles.header}>
          <Dropdown overlay={ProfileMenu}>
            <div className={styles.profile}>
              <Space>
                <Avatar src={profile?.picture?.data?.url} alt='user profile' />
                <span>{profile?.name}</span>
              </Space>
            </div>
          </Dropdown>
        </Header>
        <Content className={styles.content}>
          <Button type="primary" loading={isUpdating} onClick={updateFBPhotos}>Update</Button>
        </Content>
      </Layout>
    </Layout>
  )
}

export default withPrivateRoute(Dashboard)