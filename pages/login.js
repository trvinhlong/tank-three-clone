import React, { useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import styles from '../styles/Login.module.css'
import { FBAPIContext } from './_app'
import { Button } from 'antd'

const Login = () => {
  const router = useRouter()
  const [redirecting, setRedirecting] = useState(false)
  const { isReady, api } = useContext(FBAPIContext)

  useEffect(async () => {
    if (isReady) {
      const { status } = await api.getLoginStatus()
      if (status === 'connected') {
        setRedirecting(true)
        router.push('/dashboard')
      }
    }
  }, [isReady, api])

  const onLogin = async () => {
    try {
      await api.login({ scope: "pages_read_engagement,email" })
      const profile = await api.api("me?fields=id,name,email")
      if (!['giangth2310@gmail.com', 'longtv0910@gmail.com'].includes(profile?.email)) {
        console.log("logout")
        await api.logout()
      }
      setRedirecting(true)
      router.push('/dashboard')
    } catch (error) {
      console.log(error)
    }
  }

  if (redirecting) {
    return "Redirecting"
  }

  return (
    <div className={styles.login}>
      <Button type='primary' onClick={onLogin}>Login via Facebook</Button>
    </div>
  )
}

export default Login