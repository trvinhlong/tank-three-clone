import axios from 'axios';

const config = require('./config');

const GhtkWebhook = async (req, res) => {
    axios.post('https://slack.com/api/chat.postMessage', {
        "channel": "@long_tv",
        "username": "GHTK",
        "text": JSON.stringify(req.body)}, {
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + config.slackToken
        }
    })


    res.status(200).json({"status": "OK"})
}

export default GhtkWebhook