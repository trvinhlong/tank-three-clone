import axios from 'axios'
import fs from 'fs'
import path from 'path'
import * as fsExtra from "fs-extra"

export default async (req, res) => {
  const { accessToken } = req.query
  let photos = []
  let next = `https://graph.facebook.com/v10.0/913563252113498/photos?access_token=${accessToken}&fields=images%2Cid%2Cname%2Calt_text%2Clink`
  try {
    while (next) {
      const response = await axios.get(next)
      photos = photos.concat(response.data.data)
      next = response.data.paging.next
    }
    fsExtra.emptyDirSync(path.join(process.cwd(), "dynamicPublic/images"))

    for (const photo of photos) {
      const imgUrl = photo.images[4].source
      const imgFileName = `${photo.id}.jpg`
      const imgPath = path.join(process.cwd(), "dynamicPublic/images", imgFileName)

      const response = await axios({
        method: 'GET',
        url: imgUrl,
        responseType: 'stream',
      })
  
      const w = response.data.pipe(fs.createWriteStream(imgPath));
      w.on('finish', () => {
        console.log(`Successfully downloaded file ${imgFileName} of photo ID ${photo.id}`);
      })

      photo.src = `/images/${imgFileName}`
      delete photo.images
    }

    const filePath = path.join(process.cwd(), process.env.FB_PHOTOS_FILE)
    await fs.writeFileSync(filePath, JSON.stringify(photos))
  } catch (error) {
    console.log(error)
    return res.status(500).json(error)
  }

  return res.status(200).json(photos.length)
}
