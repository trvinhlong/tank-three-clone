import { Server } from 'socket.io'

class OrderBookDelta {
  constructor(quantity, rate) {
    this.quantity = quantity
    this.rate = rate
  }
}

const randomInRange = (min, max) => {
  return (Math.random() * (max - min) + min).toFixed(6)
}

const generateOrderBook = () => {
  const [minBidQuantity, maxBidQuantity] = [0.4, 10]
  const [minAskQuantity, maxAskQuantity] = [0.4, 10]
  const [minRate, maxRate] = [0.02, 0.06]
  let bids = []
  let asks = []
  let sumAsk = 0.0
  let sumBid = 0.0
  while (sumAsk < 150) {
    const newAsk = new OrderBookDelta(
      randomInRange(minAskQuantity, maxAskQuantity),
      randomInRange(minRate, maxRate)
    )
    asks.push(newAsk)
    sumAsk += parseFloat(newAsk.quantity)
  }
  asks = asks.slice(0, -1)

  while (sumBid < 5) {
    const newBid = new OrderBookDelta(
      randomInRange(minBidQuantity, maxBidQuantity),
      randomInRange(minRate, maxRate)
    )
    bids.push(newBid)
    sumBid += newBid.quantity * newBid.rate
  }
  bids = bids.slice(0, -1)
  return {
    "marketSymbol": "ETH-BTC",
    "depth": 25,
    "sequence": 30,
    "bidDeltas": bids,
    "askDeltas": asks
  }
}

const SocketHandler = (req, res) => {
  if (res.socket.server.io) {
    console.log('Socket is already running')
  } else {
    console.log('Socket is initializing')
    const io = new Server(res.socket.server)
    res.socket.server.io = io

    io.on('connection', socket => {
      setInterval(function () {
        socket.emit('update-orderbook', {orderbook:  generateOrderBook()})}, 30000
      );
      socket.emit('update-orderbook', {orderbook:  generateOrderBook()})  
    })
  }
  res.end()
}

export default SocketHandler