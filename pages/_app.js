import '../styles/globals.css'
import { FacebookProvider, Initialize } from 'react-facebook'
import 'antd/dist/antd.css'
import Head from 'next/head'
import React from 'react'

export const FBAPIContext = React.createContext()

const withFBAPI = (WrappedComponent) => {
  return (props) => (
    <FacebookProvider appId={process.env.NEXT_PUBLIC_FB_APP_ID} chatSupport={true}>
      <Initialize>
        {({ isReady, api }) => {
          return (
            <WrappedComponent isReady={isReady} api={api} {...props} />
          )
        }}
      </Initialize>
    </FacebookProvider>

  )
}

function MyApp({ Component, pageProps, isReady, api }) {
  return (
    <FBAPIContext.Provider value={{ isReady, api }}>
      <Head>
        <title>Tanks.vn - Cửa hàng mô hình</title>
        <meta name="description" content="Cung cấp các loại kit mô hình quân sự các chủng loại 
          Địa chỉ: Hoàng Mai, Hà Nội
          Ship cod toàn quốc
          Sđt: 0857379966 - Long (34t)">
        </meta>
        <meta property="og:image" content="/images/ogimage.jpg" key="ogimage" />
      </Head>
      <Component {...pageProps} />
    </FBAPIContext.Provider>
  )
}

export default withFBAPI(MyApp)
