import React, { useEffect, useContext, useState } from 'react'
import { useRouter } from 'next/router'
import { FBAPIContext } from '../pages/_app'
import { Spin } from 'antd'

const withPrivateRoute = (WrappedComponent) => {
  return (props) => {
    const router = useRouter()
    const [loading, setLoading] = useState(true)
    const { isReady, api } = useContext(FBAPIContext)

    useEffect(async () => {
      if (isReady) {
        const { status } = await api.getLoginStatus()
        if (status === 'connected') {
          setLoading(false)
        } else {
          router.push('/login')
        }
      }
    }, [isReady, api])

    if (loading) {
      return (
        <Spin tip='Logging in...'>
          <WrappedComponent {...props} loading={loading} />
        </Spin>
      )
    }

    return <WrappedComponent {...props} loading={loading} />
  }
}

export default withPrivateRoute